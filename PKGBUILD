# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgname=DeskDockServer
pkgname=deskdock-server
pkgver=1.3.0
pkgrel=1.3
pkgdesc="Android smartphone control via desktop"
arch=('any')
url="https://www.fdmobileinventions.com/deskdock-server"
license=(custom)
makedepends=('imagemagick')
source=("https://files.fdmobileinventions.com/${_pkgname}/${pkgver}/${_pkgname}_${pkgver}.zip"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png")
sha512sums=('0c70fc58c0612fee5fb37a42e8d85aae946440c3a248288af2136dd1e69240f06450247693327577055c2841338d9084d16250f675e15f2ce5aab3fa28fd0379'
            '661fb9ec913195de7f62163a0271faaa2b9b394e0997bbf296f92ccbbd658862d10239526b48f837d7dd15bd52a91fdfac9ebe339f074dc32e8c81f93efee20c'
            '7b9b6f200dd17e3495234f6bd03cbb9d0f9b564981a0ae9f595742ec384edd262c6b2d6d1312cd0593434c1a692dc96ddef247fd0afbd4c72cdda2ae37d82b1e')

_deskdock_desktop="[Desktop Entry]
Name=DeskDock Server
Comment=Android smartphone control via desktop
Comment[ar]=                                  التحكم في هاتف Android الذكي عبر سطح المكتب
Comment[bg]=Управление на смартфон с Android през десктоп
Comment[bs]=Upravljanje Android pametnim telefonom preko desktopa
Comment[cs]=Ovládání smartphonu Android přes desktop
Comment[da]=Android smartphone kontrol via desktop
Comment[de]=Android-Smartphone-Steuerung über Desktop
Comment[el]=Έλεγχος smartphone Android μέσω επιτραπέζιου υπολογιστή
Comment[en_GB]=Android smartphone control via desktop
Comment[en_ZA]=Android smartphone control via desktop
Comment[es]=Control de teléfonos inteligentes Android a través del escritorio
Comment[et]=Androidi nutitelefoni juhtimine töölaua kaudu
Comment[fi]=Android-älypuhelimen ohjaus työpöydän kautta
Comment[fr]=Contrôle du smartphone Android via le bureau
Comment[fr-ca]=Contrôle du smartphone Android via le bureau
Comment[he]=                             שליטה בסמארטפון אנדרואיד דרך שולחן העבודה
Comment[hr]=Upravljanje Android pametnim telefonom putem radne površine
Comment[hu]=Android okostelefon vezérlés asztali számítógépről
Comment[id]=Kontrol ponsel cerdas Android melalui desktop
Comment[is]=Android snjallsímastjórnun í gegnum skjáborð
Comment[it]=Controllo smartphone Android tramite desktop
Comment[ja]=デスクトップ経由のAndroidスマートフォン制御
Comment[km]=ការគ្រប់គ្រងស្មាតហ្វូន Android តាមរយៈកុំព្យូទ័រលើតុ
Comment[ko]=데스크탑을 통한 Android 스마트폰 제어
Comment[nl]=Android-smartphonebediening via desktop
Comment[pl]=Sterowanie smartfonem z systemem Android za pośrednictwem komputera stacjonarnego
Comment[pt]=Controle de smartphone Android via desktop
Comment[pt_BR]=Controle de smartphone Android via desktop
Comment[ro]=Control smartphone Android prin desktop
Comment[ru]=Управление Android-смартфоном через рабочий стол
Comment[sk]=Ovládanie smartfónu Android cez plochu
Comment[sl]=Nadzor pametnega telefona Android preko namizja
Comment[tr]=Masaüstü üzerinden Android akıllı telefon kontrolü
Comment[uk]=Керування Android смартфоном через робочий стіл
Comment[vi]=Điều khiển điện thoại thông minh Android qua máy tính để bàn
Exec=java -jar /opt/${_pkgname}/${_pkgname}_${pkgver}.jar
Icon=deskdock-server
Type=Application
StartupNotify=true
Categories=Utility;"

build() {
    cd "${srcdir}"
    echo -e "$_deskdock_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=('java-environment>=8' 'android-udev')

    cd "${srcdir}"
    mkdir -p "${pkgdir}/opt/${_pkgname}"
    rm -rf ${_pkgname}_${pkgver}/{mac,win}
    mv ${_pkgname}_${pkgver}/* "${pkgdir}/opt/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
